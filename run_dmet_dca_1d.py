import numpy as np
import sys
import os
sys.path.append('/home/josh/Documents/dmet_code/my_code/translationally_invariant')
import dmet_TransInv_DCA_v2
import dca_basis_one_negk
sys.path.append('/home/josh/Documents/utilities')
import make_hams
import utils

def main():

    print 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    print 'DID YOU REMEMBER TO SOURCE PRELOADMKL.SH????'
    print 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    print
    print

    Nbasis    = 8
    Nimp      = 2

    Nelec_tot = Nbasis #Half-filled Hubbard

    U         = 1.0
    boundary  = 1.0

    Hubbard   = True
    Full      = False
    HubZero   = 0

    SolverType = 'FCI'

    diag_umat = True

    h_site, V_site = make_hams.make_1D_hubbard( Nbasis, U, boundary, Full )

    #DCA_rot        = dca_basis_one_negk.dca_basis_1D( Nbasis, Nimp )
    #DCA_rot        = np.eye(Nbasis, dtype=float)
    DCA_rot        = None

    DMRG_Instruction = np.array([ 1e-7, 20, 50])

    dmetcalc       = dmet_TransInv_DCA_v2.dmet( Nbasis, Nelec_tot, Nimp, h_site, V_site, SolverType, diag_umat, Hubbard, U, HubZero, DCA_rot, DMRG_Instruction )

    Esite, dbl_occ, homo_lumo_gap, Cspin, umat    = dmetcalc.solve_groundstate()

    print 'Esite = ', Esite
    print
    print 'Double Occupancy = ', dbl_occ
    print
    print 'Homo Lumo Gap = ', homo_lumo_gap
    print
    print 'umat = ', umat

    utils.printarray( umat, 'umat.dat', True )

if __name__ == '__main__':
    main()
