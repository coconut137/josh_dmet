import numpy as np
import utils

def dca_basis_1D( Nsite, Nimp ):

    Nclst = Nsite/Nimp

    #Calculate FT matrix associated with full latice translations
    umat = np.zeros( [Nsite,Nsite], dtype=complex )
    for x in range(Nsite):
        for k in range(Nsite):
            kval = 2.0*np.pi/Nsite * ( k - 0.5 * ( Nsite/Nimp-1 ) )
            umat[x,k] = 1.0/np.sqrt(Nsite) * exp( 1j*kval*x )

    #Calculate combined back FT matrix associated with inter- and intra-cluster translations
    vwmat = np.zeros( [Nsite,Nsite], dtype=complex )
    for xtilde_indx in range(Nclst):
        for xbig_indx in range(Nimp):
            for kbig_indx in range(Nimp):
                for ktilde_indx in range(Nclst):

                    xbig   = xbig_indx
                    xtilde = xtilde_indx * Nimp
                    x      = xbig + xtilde

                    kbig_val   = kbig_indx * 2.0 * np.pi / Nimp #Symmetric kbig
                    ktilde_val = ( ktilde_indx - 0.5 * ( Nclst-1 ) ) * 2 * np.pi / Nsite #Asymmetric ktilde
                    k          = kbig_indx*Nclst + ktilde_indx

                    vwmat[x,k] = 1/np.sqrt(Nsite) * exp( 1j * ( ktilde_val*xtilde + kbig_val*xbig ) )


    #Combine the two FT matrices to form the DCA transformation matrix
    dca_rot = np.dot( umat, vwmat.conjugate().transpose() )

    #Check to see if DCA transformation matrix is real
    if( np.all( dca_rot.imag < 1e-13 ) ):
        dca_rot = dca_rot.real
    else:
        print 'ERROR: DCA rotation matrix is not real'
        exit()

    return dca_rot
#    return dca_rot, umat, vwmat #PING

def exp(x):
    return np.exp(x)
