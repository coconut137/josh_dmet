#THIS CODE ALLOWS FOR THE DCA TRANSFMORATION FOR DMET ON TRANSLATIONALLY INVARIANT SYSTEMS USING THE PRL-LIKE ALGORITHM
#THIS VERSION CAN CALL A DMRG OR FCI SOLVER FROM CHEMPS2
#THIS VERSION CALCULATES CONTRIBUTION FROM THE CORE TO THE CLUSTER ENERGY FOR THE DCA TRANSFORMED COULOMB
#HAVE THE CHOICE TO INCLUDE COULOMB ON BATH FOR EMBEDDING HAMILTONIAN IF USING THE LOCAL COULOMB
#THIS VERSION CALCULATES THE DOUBLE OCCUPANCY, THE SINGLE PARTICLE HOMO-LUMO GAP (in hartreefock module),
#AND THE SPIN-SPIN CORRELATION FUNCTION (in SolveCorrelated module), BUT ONLY FOR DMRG NOT FCI IMPURITY SOLVER
#THIS VERSION CAN SEND A SPECIFIED SET OF INSTRUCTIONS FOR DMRG
#THE DCA ALGORITHMIC CHOICES ARE:
    #SYMMETRIC RANGE OF INTER-CLUSTER MOMENTA
    #ASYMMETRIC RANGE OF INTRA-CLUSTER MOMENTA
    #BASED ON THIS CHOICE OF RANGE OF MOMENTA N/Nimp MUST BE ODD/EVEN FOR PERIODIC/ANTI-PERIODIC LATTICE
    #HAVE THE CHOICE TO USE ORIGINAL LOCAL COULOMB OR DCA TRANSFORMED COULOMB
#THE DMET ALGORITHMIC CHOICES IN THIS CODE ARE:
    #COMPARED TO FIRST VERSION, THIS VERSION USES PRL ALGORITHM FOR HUBBARD MODEL ie not CASCI and add u_mat to bath before rotating H to embedding basis
    #THIS VERSION HAS FLAG TO FIX DIAGONAL MATRICES OF u-matrix TO BE THE SAME
    #THIS VERSION IS ANALOGOUS TO SEBASTIAN/TIM WHERE WHEN OPTIMIZING u-matrix THE ONE ELECTRON INTEGRALS ON THE BATH-SITES ARE FIXED AT ALL STEPS
    #AND THUS DIAGONALIZE ONLY IN IMPURITY + BATH SPACE
    #MINIMIZE DIFFERENCE BETWEEN 1RDM ON IMPURITY AND BATH


import time
import numpy as np
from scipy.optimize import minimize
import sys
import os
sys.path.append('/home/josh/Documents/utilities')
import hartreefock as hf
import utils
import SolveCorrelated


class dmet:

    def __init__( self, Nbasis, Nelec_tot, Nimp, h_site, V_site=None, SolverType='FCI', diag_umat=False, Hubbard=False, U=None, HubZero=0, DCA_rot=None, DMRG_Instruction=None, u_matrix=None ):

        print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        print "BEGIN DMET CALCULATION"
        print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        print
        print "--------------------------------------------------------"
        print "STARTING PARAMETERS:"
        print
        print "Total number of sites     = ",Nbasis
        print "Number of impurity sites  = ",Nimp
        print "Total number of electrons = ",Nelec_tot
        if( ( Nbasis/Nimp ) % 2 == 1 ):
            print "N / Nimp is odd"
        else:
            print "N / Nimp is even"
        if( SolverType == 'FCI' ):
            print "Impurity Solver - FCI"
        elif( SolverType == 'DMRG' ):
            print "Impurity Solver - DMRG"
        if( DCA_rot == None ):
            print "Not running with DCA and"
        else:
            print "Running with DCA and"
        if( HubZero == 0 ):
            print "using local coulomb and setting 2 e- terms to zero on bath in embedding basis"
        elif( HubZero == 1 ):
            print "using local coulomb and rotating full 2 e- terms including bath to embedding basis"
        elif( HubZero == 2 ):
            print "using DCA transformed coulomb and rotating full 2 e- terms including bath to embedding basis"
        elif( HubZero == 3 ):
            print "using course-grained DCA transformed coulomb and rotating full 2 e- terms including bath to embedding basis"
        print "--------------------------------------------------------"
        print

        #Note: self.h_site and self.V_site are given in original site basis, not DCA basis

        self.Nbasis     = Nbasis    #Number of basis functions
        self.Nelec_tot  = Nelec_tot #Number of electrons in the total system
        self.h_site     = h_site    #One electron integrals
        self.V_site     = V_site    #Two electron integrals
        self.Nimp       = Nimp   #Array of arrays containing indices of impurity orbitals for each cluster

        self.SolverType = SolverType #String stating whether using FCI or DMRG as the impurity solver

        self.diag_umat  = diag_umat #Logical stating whether to constrain the diagonal elements of the u-matrix to be the same w/in a cluster

        self.Hubbard    = Hubbard #Logical stating whether running Hubbard model to use simplifications of code
        self.HubZero    = HubZero #Integer defining how to treat the 2e- terms in the embedding basis
        self.U          = U #Value of Hubbard-U parameter

        self.DCA_rot    = DCA_rot #Rotation matrix from original site-basis to DCA site-basis

        self.DMRG_Instruction = DMRG_Instruction #Array specifying the instructions for a DMRG calculation

        if( self.HubZero == 2):
            self.h_ImpCore_emb  = np.array([Nimp,Nimp]) #Matrix that contains interaction between core and impurity

        if( self.DCA_rot != None ):
            #Form 1e- terms in DCA basis
            self.h_DCA = utils.rot1el( self.h_site, self.DCA_rot )

        #ERROR CHECKS:
        if( self.Hubbard and self.U == None ):
            print "ERROR: Running Hubbard model, but did not provide value for Hubbard-U"
            exit()

        if( self.Nelec_tot % 2 !=0 ):
            print "ERROR: Number of total electrons not even"
            exit()

        if( self.Nbasis % Nimp != 0 ):
            print "ERROR: Number of impurities does not tesselate the full lattice"
            exit()

        if( ( self.SolverType != 'FCI' ) and ( self.SolverType != 'DMRG' ) ):
            print "ERROR: SolverType was not given as 'FCI' or 'DMRG'"
            exit()

        if( self.DCA_rot != None ):
            #now checking this when making the DCA rotation matrix
            #if( ( ( self.Nbasis/Nimp ) % 2 != 1 ) and ( Nimp != 1 ) ):
            #    print "ERROR: Running DCA, but N/Nimp is not odd"
            #    exit()
            if( self.HubZero > 3 ):
                print "ERROR: Running DCA, but HubZero > 3"
                exit()
        else:
            if( self.HubZero > 1 ):
                print "ERROR: Not running DCA, but HubZero > 1"
                exit()   


        #Set-up u-matrix:
        self.u_mat = np.zeros( [Nbasis,Nbasis] )

        if( u_matrix == None ):
            u_matrix  = np.zeros( [Nimp,Nimp] )
            if( self.Hubbard ):
                for i in range(Nimp):
                    u_matrix[i,i] = 0.5 * self.U * self.Nelec_tot / self.Nbasis

        self.u_mat     = self.replicate_u_matrix( u_matrix )
        self.u_mat_new = np.copy( self.u_mat )       #Initialize copy of u-matrix


    #####################################################################

    def get_rotmat( self, env1RDM ):

        #Subroutine to generate rotation matrix from site to embedding basis
        #rotation matrix is of form ( site basis fcns ) x ( impurities, bath )

        #remove rows/columns of 1RDM corresponding to impurity sites to generate environment 1RDM
        for imp in range(self.Nimp):

            env1RDM  = np.delete( env1RDM, 0, axis=0 )
            env1RDM  = np.delete( env1RDM, 0, axis=1 )

        #diagonalize environment 1RDM to define bath orbitals w/evals btwn 0 and 2, and core orbitals w/evals=2
        evals, evecs = utils.diagonalize( env1RDM )

        #remove environment orbitals with zero occupancy (virtual space) and core orbitals w/evals=2, leaving only bath
        Nocc = self.Nelec_tot/2
        evecs = evecs[ :, -Nocc:-Nocc+self.Nimp ]

        #form rotation matrix consisting of unit vectors for impurity and eigenvectors for just bath ( no core )
        rotmat = np.zeros( [ self.Nbasis, self.Nimp ] )
        for imp in range(self.Nimp):

            rotmat[ imp, imp ] = 1.0

            evecs = np.insert( evecs, imp, 0.0, axis=0 )

        rotmat = np.concatenate( (rotmat,evecs), axis=1 )

        return rotmat

    #####################################################################

    def get_embedding_ham( self, rotmat ):

        #augment 1e- terms w/u-matrix only on bath because not doing casci
        #note, the following works b/c u_mat is block diagonal for each cluster
        #if doing DCA, h_curr is given in the DCA site basis otw given in original site basis
        if( self.DCA_rot == None ):
            h_curr = np.copy( self.h_site )
        else:
#            h_curr = utils.rot1el( self.h_site, self.DCA_rot )
            h_curr = np.copy( self.h_DCA )
        h_curr[ self.Nimp:, self.Nimp: ] += self.u_mat[ self.Nimp:, self.Nimp: ]

        #rotate the 1/2 e- terms into embedding basis using rotation matrix
        #note, if doing dca it is the dca embedding basis
        h_emb = utils.rot1el( h_curr, rotmat )
        if( self.Hubbard ):
            if( self.DCA_rot == None ):
                if( self.HubZero == 0 ):
                    #Set 2-electron terms on the bath to zero in embedding basis
                    V_emb = np.zeros([2*self.Nimp,2*self.Nimp,2*self.Nimp,2*self.Nimp])
                    for i in range(self.Nimp):
                        V_emb[i,i,i,i] = self.U
                elif( self.HubZero == 1 ):
                    #Rotate the full 2-electron terms including the bath to embedding basis
                    V_emb = np.einsum( 'wr,vr,rx,ry -> wvxy', rotmat.conj().T[:2*self.Nimp,:], rotmat.conj().T[:2*self.Nimp,:], rotmat[:,:2*self.Nimp], rotmat[:,:2*self.Nimp] )
                    V_emb *= self.U
            else:
                if( self.HubZero == 0 ):
                    #Coarse-grained DCA result returns the same local Hubbard Coulomb interaction
                    #Set 2-electron terms on the bath to zero in embedding basis
                    V_emb = np.zeros([2*self.Nimp,2*self.Nimp,2*self.Nimp,2*self.Nimp])
                    for i in range(self.Nimp):
                        V_emb[i,i,i,i] = self.U
                elif( self.HubZero == 1 ):
                    #Course-grained DCA result returns the same local Hubbard Coulomb interaction
                    #Rotate the full 2-electron terms including the bath to embedding basis
                    V_emb = np.einsum( 'wr,vr,rx,ry -> wvxy', rotmat.conj().T[:2*self.Nimp,:], rotmat.conj().T[:2*self.Nimp,:], rotmat[:,:2*self.Nimp], rotmat[:,:2*self.Nimp] )
                    V_emb *= self.U
                elif( self.HubZero == 2 ):
                    #DCA basis transformation of 2e- terms without course-graining
                    #Rotate the full 2-electron terms including the bath to embedding basis
                    cmbd_mat = np.dot( self.DCA_rot, rotmat )
                    V_emb = np.einsum( 'ar,br,rc,rd -> abcd', cmbd_mat.conj().T[:2*self.Nimp,:], cmbd_mat.conj().T[:2*self.Nimp,:], cmbd_mat[:,:2*self.Nimp], cmbd_mat[:,:2*self.Nimp] )
                    V_emb *= self.U

                    #Additionally need to calculate contribution to 1 e- terms btwn core and impurity necessary to calculate cluster energy
                    #Don't need contribution btwn core and bath, b/c this does not appear in calculation of cluster energy
                    mat1 = 2 * self.U * np.einsum( 'vr,er,rw,re -> vw', cmbd_mat.conj().T[:self.Nimp,:], cmbd_mat.conj().T[self.Nimp:,:], cmbd_mat[:,:self.Nimp], cmbd_mat[:,self.Nimp:] )
                    mat2 = self.U * np.einsum( 'vr,er,re,rw -> vw', cmbd_mat.conj().T[:self.Nimp,:], cmbd_mat.conj().T[self.Nimp:,:], cmbd_mat[:,self.Nimp:], cmbd_mat[:,:self.Nimp] )
                    self.h_ImpCore_emb = mat1 - mat2

                #elif( self.HubZero == 3 ):
                    #DCA basis transformation of 2e- terms followed by course-graining
                    #Rotate the full 2-electron terms including the bath to embedding basis
        else:
            V_emb = utils.rot2el_phys( self.V_site, rotmat )

        return h_emb, V_emb

    #####################################################################

    def get_properties( self, h_emb, V_emb, corr1RDM, corr2RDM ):

        #Calculate Site Energy

        Eclust = 0.0

        if( self.HubZero == 2 ):
            #Augment the one 1 e- terms with interaction btwn core and impurity for DCA basis transformation
            h_emb[ :self.Nimp, :self.Nimp ] += self.h_ImpCore_emb

        for orb1 in range(self.Nimp):
            for orb2 in range(2*self.Nimp):
                Eclust += corr1RDM[ orb1, orb2 ] * h_emb[ orb1, orb2 ]
                for orb3 in range(2*self.Nimp):
                    for orb4 in range(2*self.Nimp):
                        Eclust += 0.5 * corr2RDM[ orb1, orb2, orb3, orb4 ]*V_emb[ orb1, orb2, orb3, orb4 ]

        Esite = Eclust / self.Nimp


        #Calculate Double Occupancy

        dbl_occ = 0.0

        for orb1 in range(self.Nimp):
            dbl_occ += 0.5 * corr2RDM[ orb1, orb1, orb1, orb1 ]

        dbl_occ = dbl_occ / self.Nimp

        return Esite, dbl_occ

    #####################################################################

    def minimize_u_matrix( self, h_emb, corr1RDM ):

        #Define parameters to optimize from previous DMET guess (vs guess from previous cluster in the loop ) of u-matrix associated with impurity sites in cluster
        #Note since u-matrix must be symmetric (if real) only optimize half the parameters
        #Note if constraining diagonal of u-matrix, first index of params will be the diagonal term, and the rest the upper triagonal of the u-matrix
        u_mat_imp = self.u_mat[ :self.Nimp, :self.Nimp ]
        params = self.matrix2array( u_mat_imp )

        #Minimize difference between HF and correlated DMET 1RDMs
        min_result = minimize( self.cost_function, params, args=(h_emb, corr1RDM) )

        #Update new u-matrix from optimized parameters
        u_mat_imp = self.array2matrix( min_result.x )
        self.u_mat_new = self.replicate_u_matrix( u_mat_imp )

    #####################################################################

    def cost_function( self, params, h_emb, corr1RDM ):

        #Calculate modified 1e- terms associated only w/impurity+bath space in embedding basis
        #Note adding the u_mat only to the 1e- terms on the impurity, the bath remains fixed throughout the minimization
        #Note since embedding and site ( or DCA) basis are the same on the impurity can just add umat to h_site ( or h_DCA )
        u_mat_imp = self.array2matrix( params )
        if( self.DCA_rot == None ):
            h_emb[ :self.Nimp, :self.Nimp ] = self.h_site[ :self.Nimp, :self.Nimp ] + u_mat_imp
        else:
            h_emb[ :self.Nimp, :self.Nimp ] = self.h_DCA[ :self.Nimp, :self.Nimp ] + u_mat_imp

        #Run HF using modified 1 e- terms to generate HF RDM in embedding basis
        #Note only diagonalizing the impurity+bath space as is done in sebastian/tim's code
        hf1RDM_emb, garbage = hf.rhf_calc_hubbard( 2*self.Nimp, h_emb )

        #return the magnitude of the square of the difference of the HF and correlated 1 RDMs
        #currently include contributions from impurity and bath
        return np.linalg.norm( corr1RDM - hf1RDM_emb )**2

    #####################################################################
    
    def matrix2array( self, mat ):

        if( self.diag_umat ):
            array = mat[ np.triu_indices( len(mat),1 ) ]
            array = np.insert(array, 0, mat[0,0])
        else:
            array = mat[ np.triu_indices( len(mat) ) ]

        return array

    #####################################################################

    def array2matrix( self, array ):

        if( self.diag_umat ):
            dim = (1.0+np.sqrt(1-8*( 1-len(array) )))/2.0
        else:
            dim = (-1.0+np.sqrt(1+8*len(array)))/2.0

        mat = np.zeros( [dim,dim] )

        if( self.diag_umat ):
            mat[ np.triu_indices(dim,1) ] = array[1:]
            np.fill_diagonal( mat, array[0] )
        else:
            mat[ np.triu_indices(dim) ] = array

        mat = mat + mat.conjugate().transpose() - np.diag(np.diag(mat))

        return mat

    #####################################################################

    def replicate_u_matrix( self, u_imp ):

        #Number of copies of impurity cluster in total lattice
        Ncopies = self.Nbasis / self.Nimp

        #Copy impurity u-matrix across entire lattice
        u_mat_replicate = np.zeros( [self.Nbasis,self.Nbasis] )
        for cpy in range(0,Ncopies):
            for orb1 in range(self.Nimp):
                indx1 = ( orb1+self.Nimp*cpy ) % self.Nbasis
                for orb2 in range(self.Nimp):
                    indx2 = ( orb2+self.Nimp*cpy ) % self.Nbasis
                    u_mat_replicate[indx1,indx2] = u_imp[orb1,orb2]

        return u_mat_replicate

    #####################################################################

    def solve_groundstate( self ):

        #DMET loop to converge u-matrix
        tol        = 1e-6 * self.Nimp
        u_mat_diff = tol+1
        itr        = 0
        itrmax     = 200
        while( u_mat_diff > tol ):

            #Note: self.h_site and self.V_site are given in original site basis, not DCA basis
            #Note: self.u_mat and self.u_mat_new are given in the DCA basis not original site basis

            itr += 1

            #Calculate modified 1e- terms in site basis
            if( self.DCA_rot == None ):
                h_site_mod = self.h_site+self.u_mat
            else:
                #Define h_site_mod in original site basis, thus need to rotate u_mat from DCA back to original site basis
                h_site_mod = self.h_site + utils.rot1el( self.u_mat, self.DCA_rot.conjugate().transpose() )

            #Run HF using modified 1 e- and original 2 e- terms to generate HF orbitals in original site basis
            if( self.Hubbard ):
                hf1RDM_site, homo_lumo_gap = hf.rhf_calc_hubbard( self.Nelec_tot, h_site_mod )
            else:
                hf1RDM_site, hforbs, hfE, hfevals = hf.rhf_calc( self.Nbasis, self.Nelec_tot, h_site_mod, self.V_site )

            #Rotate hf1RDM to DCA basis
            if( self.DCA_rot != None ):
                hf1RDM_site = utils.rot1el( hf1RDM_site, self.DCA_rot )

            #Calculate rotation matrix to embedding basis from HF 1RDM (same as calculating bath orbitals)
            #For DCA, rotmat goes from DCA site basis to DCA embedding basis
            rotmat = self.get_rotmat( hf1RDM_site )

            #Calculate 1/2e- terms in embedding basis
            h_emb, V_emb = self.get_embedding_ham( rotmat ) 

            t1 = time.time()
            #Use 1e-/2e- terms to do correlated calculation in embedding basis using CheMPS2
            corr1RDM, corr2RDM, Cspin = SolveCorrelated.chemps2_solveGS( 2*self.Nimp, 2*self.Nimp, h_emb, V_emb, self.SolverType, self.DMRG_Instruction )
            t2 = time.time()
            time_for_corr = t2 - t1

            #Truncate array of spin-spin correlation fcns to just between impurity sites
            Cspin = Cspin[ :self.Nimp, :self.Nimp ]

            #Calculate the DMET site energy
            Esite, dbl_occ = self.get_properties( h_emb, V_emb, corr1RDM, corr2RDM )

            #udpate new u-matrix associated with contribution from current cluster by minimizing difference between HF and DMET 1RDMs in embedding basis
            t1 = time.time()
            self.minimize_u_matrix( h_emb, corr1RDM )
            t2 = time.time()
            time_for_umat = t2 - t1

            #calculate frobenius norm of difference between old and new u-matrix (np.linalg.norm)
            u_mat_diff = np.linalg.norm( self.u_mat - self.u_mat_new )

            #update u-matrix with most recent guess
            self.u_mat = self.u_mat_new

            print 'FINISHED DMET ITERATION ',itr
            print '    printing current estimate for the u-matrix'
            utils.printarray( self.u_mat[ :self.Nimp, :self.Nimp ], 'dmet_u_mat_current.dat', True )
            print '    current value of difference in u-MATRIX = ',u_mat_diff
            print '    current estimate of the sight energy    = ',Esite
            print '    time for correlated calculation         = ',time_for_corr
            print '    time to find new u-matrix               = ',time_for_umat

            if( itr >= itrmax ):
                print "u-Matrix did not converge in less than", itrmax, "iterations"
                exit()

        print
        print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        print "END DMET CALCULATION"
        print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        print

        return Esite, dbl_occ, homo_lumo_gap, Cspin, self.u_mat_new[ :self.Nimp, :self.Nimp ]

#########################################################################



