import numpy as np
import ctypes
import sys
import os
sys.path.append('/home/josh/Documents/chemps2/build/PyCheMPS2')
import PyCheMPS2
import utils

#########################################################################

def chemps2_solveGS( Norbs, NelecActiveSpace, h_mat, V_mat, SolverType, DMRG_Instruction=None, outputCI=False, Econst=0.0, printoutput=False ):

    #Subroutine to use CheMPS2 to solve for FCI groundstate given 1/2 e- integrals

    #Set the seed of the random number generator and cout.precision
    Initializer = PyCheMPS2.PyInitialize()
    Initializer.Init()

    #Setting up the Hamiltonian assuming C1 symmetry
    Group = 0
    orbirreps = np.zeros([Norbs], dtype=ctypes.c_int)
    HamCheMPS2 = PyCheMPS2.PyHamiltonian(Norbs, Group, orbirreps)
    HamCheMPS2.setEconst( Econst )
    for cnt1 in range(Norbs):
        for cnt2 in range(Norbs):
            HamCheMPS2.setTmat( cnt1, cnt2, h_mat[cnt1,cnt2] )
            for cnt3 in range(Norbs):
                for cnt4 in range(Norbs):
                    HamCheMPS2.setVmat( cnt1, cnt2, cnt3, cnt4, V_mat[cnt1,cnt2,cnt3,cnt4] )

    #Killing output if necessary
    if ( printoutput==False ):
        sys.stdout.flush()
        old_stdout = sys.stdout.fileno()
        new_stdout = os.dup(old_stdout)
        devnull = os.open('/dev/null', os.O_WRONLY)
        os.dup2(devnull, old_stdout)
        os.close(devnull)

    if( SolverType == 'FCI' ):

        #FCI ground state calculation
        assert( NelecActiveSpace % 2 == 0 )
        Nel_up       = NelecActiveSpace / 2
        Nel_down     = NelecActiveSpace / 2
        Irrep        = 0
        maxMemWorkMB = 100.0
        FCIverbose   = 2
        theFCI = PyCheMPS2.PyFCI( HamCheMPS2, Nel_up, Nel_down, Irrep, maxMemWorkMB, FCIverbose )
        GSvector = np.zeros( [ theFCI.getVecLength() ], dtype=ctypes.c_double )
        theFCI.FillRandom( theFCI.getVecLength() , GSvector )
        GSenergy = theFCI.GSDavidson( GSvector )
        SpinSquared = theFCI.CalcSpinSquared( GSvector )
        GS_2RDM = np.zeros( [ Norbs**4 ], dtype=ctypes.c_double )
        theFCI.Fill2RDM( GSvector, GS_2RDM )
        GS_2RDM = GS_2RDM.reshape( [Norbs, Norbs, Norbs, Norbs], order='F' )
        GS_1RDM = np.einsum( 'ikjk->ij', GS_2RDM ) / ( NelecActiveSpace - 1 )
        Cspin = np.zeros( [Norbs,Norbs], dtype=ctypes.c_double ) #Return empty array for spin-spin correlation function

        #Check if the ground state was a spin singlet
        if ( abs(SpinSquared) > 1e-8 ):
            print "Exact solution :: WARNING : < S^2 > =", SpinSquared

    else:

        #DMRG ground state calculation
        TwoS  = 0
        Irrep = 0
        Prob  = PyCheMPS2.PyProblem( HamCheMPS2, TwoS, NelecActiveSpace, Irrep )

        OptScheme = PyCheMPS2.PyConvergenceScheme(3) # 3 instructions
        if( DMRG_Instruction == None ):
            Econv = 1e-7
            D1 = 500
            D2 = 1000
        else:
            Econv = DMRG_Instruction[ 0 ]
            D1 = int( DMRG_Instruction[ 1 ] )
            D2 = int( DMRG_Instruction[ 2 ] )

        #OptScheme.setInstruction(instruction, D, Econv, maxSweeps, noisePrefactor)
        OptScheme.setInstruction(0, D1, Econv,  3, 0.05)
        OptScheme.setInstruction(1, D2, Econv,  3, 0.05)
        OptScheme.setInstruction(2, D2, Econv, 10, 0.00) # Last instruction a few iterations without noise

        theDMRG = PyCheMPS2.PyDMRG( Prob, OptScheme )
        EnergyCheMPS2 = theDMRG.Solve()
        theDMRG.calc2DMandCorrelations()
        GS_2RDM = np.zeros( [ Norbs,Norbs,Norbs,Norbs ], dtype=ctypes.c_double )
        Cspin = np.zeros( [Norbs,Norbs], dtype=ctypes.c_double )
        for orb1 in range(Norbs):
            for orb2 in range(Norbs):
                Cspin[ orb1, orb2 ] = 0.25 * theDMRG.getCspin( orb1, orb2 ) #Spin-spin correlation fcn <si^z sj^z> - <si^z><sj^z>
                for orb3 in range(Norbs):
                    for orb4 in range(Norbs):
                        GS_2RDM[ orb1, orb2, orb3, orb4 ] = theDMRG.get2DMA( orb1, orb2, orb3, orb4 ) #All in physics notation ie <a_orb1^dag a_orb2^dag a_orb4 a_orb3>
        GS_1RDM = np.einsum( 'ikjk->ij', GS_2RDM ) / ( NelecActiveSpace - 1 ) #Based on definition of 2RDM, the 1RDM is given as <ai^dag aj>

        # theDMRG.deleteStoredMPS()
        theDMRG.deleteStoredOperators()
        del theDMRG
        del OptScheme
        del Prob

    del HamCheMPS2

    #Reviving output if necessary
    if ( printoutput==False ):
        sys.stdout.flush()
        os.dup2(new_stdout, old_stdout)
        os.close(new_stdout)

    if( outputCI ):
        return GS_1RDM, GS_2RDM, Cspin, GSvector
    else:
        return GS_1RDM, GS_2RDM, Cspin

#########################################################################




