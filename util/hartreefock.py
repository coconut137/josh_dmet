#!/usr/bin/python

import numpy as np
import scipy.linalg as la
from utils import *

#####################################################################

def rhf_calc_hubbard(Nelec,Hcore):

    #simplified subroutine to perform a mean-field (ie U=0) calculation for Hubbard model

    #Diagonalize hopping-hamiltonian
    evals,orbs = diagonalize(Hcore)

    #Form the 1RDM
    P = rdm_1el(orbs,Nelec/2)

    #Calculate Homo-Lumo Gap
    homo_lumo_gap = evals[Nelec/2] - evals[Nelec/2-1]

    return P, homo_lumo_gap

#####################################################################

def rhf_calc(Ns,Nelec,Hcore,g,S=None):

    #subroutine to perform a hartree-fock calculation
    #param int Ns: number of spatial atomic orbitals ("sites")
    #param numpy.ndarray g: four-index tensor of 2-el integrals in AO (site) basis

    #Initialize over-lap to identity if None provided
    if ( S == None ):
        S = np.identity(Ns)

    #generate initial guess of the density matrix assuming fock operator is given by Hcore
    evals,orbs = diagonalize(Hcore,S)
    P = rdm_1el(orbs,Nelec/2)

    itrmax = 900
    Enew = 9999.9
    Ediff = 10.0
    itr = 0
    while Ediff > 1e-8 and itr < itrmax:
        #SCF Loop:

        #calculate 2e- contribution to fock operators
        h2el = make_h2el(g,P)

        #form fock operators
        fock = Hcore+h2el

        #solve the fock equations
        evals,orbs = diagonalize(fock,S)

        #form the density matrices
        P = rdm_1el(orbs,Nelec/2)

        #calculate the new HF-energy and check convergence
        Eold = Enew
        Enew = Ehf(Hcore,fock,P)
        Ediff = np.fabs(Enew-Eold)

        #accumulate iteration
        itr += 1

    return P, orbs, Enew, evals


#####################################################################


def rdm_1el(C,Ne):
    #subroutine that calculates and returns the one-electron density matrix

    Cocc = C[:,:Ne]
    P = 2*np.dot( Cocc,np.transpose(np.conjugate(Cocc)) )

    return P

#####################################################################

def make_h2el(g,P):
    #subroutine that calculates the two-electron contribution to the fock matrix

    Pa = 0.5*P

    h2el = ( np.tensordot(P,g,axes=([0,1],[3,2])) -
             np.tensordot(Pa,g,axes=([0,1],[1,2])) )

    return h2el

#####################################################################

def Ehf(Hcore,fock,P):

    #subroutine that calculates the RHF-energy

    Ehf = 0.5*np.trace( np.dot(P,Hcore)+np.dot(P,fock) )

    #Remove imaginary part associated with what should be numerical error
    if( Ehf.imag > 1e-15 ):
        print 'ERRROR: HF energy has non-neglibigle imaginary part'
        exit()
    else:
        Ehf = Ehf.real

    return Ehf

#####################################################################

