#THIS CODE INCLUDES NECESSARY SUBROUTINES TO RUN STATIC INTERACTING BATH DMET CALCULATION PRIOR TO THE REAL-TIME DMET CALCULATION
#EVERYTHING IS IN ATOMIC UNITS
#ASSUMES CHEMISTRY NOTATION FOR THE 2 ELECTRON INTEGRALS THROUGHOUT
#THE NOTATION FOR THE 1-RDM IS AS FOLLOWS: gamma_ij = < a_j^dag a_i >

#THIS VERSION IS ONLY CAPABLE OF DOING A SINGLE IMPURITY, OF ARBITRARY SIZE
#THIS VERSION ALLOWS SPECIFICATION OF CI COEFFICIENTS IN THE EMBEDDING REGION, INSTEAD OF CALCULATING CORRELATED CALCULATION
#NOTE: IF CI COEFFICIENTS NOT DEFINED, THEY ARE RETURNED IN CHEMPS2 NOTATION, IF THEY ARE DEFINED, THEY MUST BE DEFINED (AND THEN WILL BE RETURNED) IN PYSCF NOTATION
#THIS VERSION DIAGONLIZES THE ONE ELECTRON MATRIX FOR THE "MEAN-FIELD" CALCULATION ALWAYS
#THIS VERSION HAS A SIMPLIFICATION FOR SINGLE IMPURITY ANDERSON MODEL IN DEFINING THE EMBEDDING BASIS
#THIS VERSION INCLUDES THE CONTRIBUTION FROM THE CONSTANT CORE ENERGY WHEN PERFORMING THE CORRELATED CALCULATION
#THIS VERSION DIAGONALIZES THE 1e- MATRIX FORMED SOLELY FROM THE CORE STATES OR THE VIRTUAL STATES TO DEFINE A CONSITENT SET OF CORE AND VIRTUAL ORBITALS AT EACH TIME-STEP DURING RT-DMET
#THIS VERSION RETURNS THE FINAL CORRELATION POTENTIAL

import numpy as np
import time
from scipy.optimize import minimize
import sys
import os
from pyscf.fci import cistring
import pyscf.fci
sys.path.append('/home/josh/Documents/utilities')
import hartreefock as hf
import utils
import make_hams
import SolveCorrelated

#####################################################################

def static_dmet( Nelec, Nbasis, Nimp, h_site, hamtype=0, V_site=None, SolverType='FCI', DMRG_Instruction=None, umat=None, diag_umat=False, fitbath=True, defCIcoeffs=False, CIcoeffs=None ):


    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print "BEGIN STATIC DMET CALCULATION"
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print


    #INPUT ERROR CHECKS
    if( Nelec % 2 !=0 ):
        print "ERROR: Number of total electrons not even"
        print
        exit()

    if( ( SolverType != 'FCI' ) and ( SolverType != 'DMRG' ) ):
        print "ERROR: SolverType was not given as 'FCI' or 'DMRG'"
        print
        exit()

    if( ( defCIcoeffs ) and ( CIcoeffs is None ) ):
        print "ERROR: defCIcoeffs was given as True, but no input CIcoeffs were given"
        print
        exit()

    if( defCIcoeffs ):
        #Check that input CIcoeffs matrix is of the correct size
        link_index = cistring.gen_linkstr_index( range(2*Nimp), Nimp )
        nalphastr  = link_index.shape[0]
        nbetastr   = nalphastr
        shape_chk = nalphastr, nbetastr
        if( shape_chk != CIcoeffs.shape ):
            print "ERROR: Input CI coefficient matrix is of the wrong dimensions"
            exit() 

    if( hamtype == 0 and not hasattr( V_site, "__len__" ) ):
        print 'ERROR: Selected to run with a general Hamiltonian, but have not specified the 2e- integrals'
        print
        exit()

    if( hamtype == 1 and not isinstance( V_site, float ) ):
        print 'ERROR: Selected to run with single impurity anderson model hamiltonian, but did not correctly specify the electron repulsion term, U, correctly'
        print
        exit()

    #Set-up u-matrix
    if( umat is None ):
        umat = np.zeros( [Nimp,Nimp] )


    #DMET loop to converge u-matrix
    tol        = 1e-8 * Nimp
    umat_diff = tol+1
    itr        = 0
    itrmax     = 200
    while( umat_diff > tol ):

        itr += 1

        print 'STARTING DMET ITERATION ',itr

        #Calculate modified 1e- terms in site basis
        h_site_mod = np.copy( h_site )
        h_site_mod[ :Nimp, :Nimp ] += umat

        #Run mean-field calculation using modified hamiltonian
        mf1RDM_site, homo_lumo_gap = hf.rhf_calc_hubbard( Nelec, h_site_mod )

        #Calculate rotation matrix to embedding basis from HF 1RDM (same as calculating bath orbitals)
        rotmat, emb_vals = get_emb_basis( Nelec, Nbasis, Nimp, mf1RDM_site, h_site )

        #Calculate 1/2e- terms in the embedding basis
        h_emb, V_emb, Ecore = get_emb_ham( Nimp, Nelec, rotmat, h_site, hamtype, V_site )

        if( defCIcoeffs ):
            #Calculate correlated 1RDM using PYSCF and given CIcoeffs if CIcoeffs are given
            corr1RDM = pyscf.fci.direct_spin1.make_rdm1( CIcoeffs, 2*Nimp, 2*Nimp )
        else:
            #Use 1e-/2e- terms to do correlated ground-state calculation in embedding basis using CheMPS2 if CIcoeffs are not given
            corr1RDM, corr2RDM, Cspin, CIcoeffs = SolveCorrelated.chemps2_solveGS( 2*Nimp, 2*Nimp, h_emb, V_emb, SolverType, DMRG_Instruction, True, Ecore )

            #Truncate array of spin-spin correlation fcns to just between impurity sites
            Cspin = Cspin[ :Nimp, :Nimp ]

        #Calculate DMET properties (haven't included this yet)
        #Esite, dbl_occ = get_properties( h_emb, V_emb, corr1RDM, corr2RDM )

        #get new correlation potential by minimizing difference between HF and DMET 1RDMs in embedding basis
        umat_new = get_new_umat( umat, Nelec, Nbasis, Nimp, h_site, corr1RDM, rotmat, diag_umat, fitbath )

        #difference between current and old correlation potentials
        umat_diff = np.linalg.norm( umat - umat_new )**2

        #update u-matrix with most recent guess
        umat = np.copy( umat_new )

        print 'FINISHED DMET ITERATION ',itr
        print '    printing current estimate for the u-matrix'
        utils.printarray( umat, 'dmet_umat_current.dat', True )
        print '    current value of difference in u-MATRIX = ',umat_diff

        if( itr >= itrmax ):
            print "u-Matrix did not converge in less than", itrmax, "iterations"
            exit()

    print
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print 'STATIC DMET CALCULATION CONVERGED'
    print 'CALCULATING FINAL SET OF DATA'

    #CALCULATE FINAL SET OF DATA

    #Calculate modified 1e- terms in site basis
    h_site_mod = np.copy( h_site )
    h_site_mod[ :Nimp, :Nimp ] += umat

    #Run mean-field calculation using modified hamiltonian
    mf1RDM_site, homo_lumo_gap = hf.rhf_calc_hubbard( Nelec, h_site_mod )

    if( not defCIcoeffs ):
        #Calculate final set of CIcoeffs if not initially defined

        #Calculate rotation matrix to embedding basis from HF 1RDM (same as calculating bath orbitals)
        rotmat, emb_vals = get_emb_basis( Nelec, Nbasis, Nimp, mf1RDM_site, h_site )

        #Calculate 1/2e- terms in the embedding basis
        h_emb, V_emb, Ecore = get_emb_ham( Nimp, Nelec, rotmat, h_site, hamtype, V_site )

        #Use 1e-/2e- terms to do correlated calculation in embedding basis using CheMPS2
        corr1RDM, corr2RDM, Cspin, CIcoeffs = SolveCorrelated.chemps2_solveGS( 2*Nimp, 2*Nimp, h_emb, V_emb, SolverType, DMRG_Instruction, True, Ecore )

        #Truncate array of spin-spin correlation fcns to just between impurity sites
        Cspin = Cspin[ :Nimp, :Nimp ]

        #Calculate DMET properties (haven't included this yet)
        #Esite, dbl_occ = get_properties( h_emb, V_emb, corr1RDM, corr2RDM )

    print "END STATIC DMET CALCULATION"
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print

    return mf1RDM_site, CIcoeffs, umat

#####################################################################

def get_emb_basis( Nelec, Nbasis, Nimp, mf1RDM, h_site ):

    #Subroutine to obtain virtual, bath and core states from the mean-field 1RDM (mf1RDM)
    #Returns the rotation matrix from the site basis to the embedding basis (ie from site to impurity+bath+core+virtual basis)
    #Returns the eigenvalues associated with the bath, virtual and core states
    #Rotation matrix is of form ( site basis fcns ) x ( impurities, virtual, bath, core )
    #Assumes the impurities are the first Nimp sites

    #remove rows/columns of 1RDM corresponding to impurity sites to generate environment 1RDM
    for imp in range(Nimp):

        mf1RDM  = np.delete( mf1RDM, 0, axis=0 )
        mf1RDM  = np.delete( mf1RDM, 0, axis=1 )

    #diagonalize environment 1RDM to define bath orbitals w/evals btwn 0 and 2, virutal orbitals w/evals=0, and core orbitals w/evals=2
    evals, evecs = utils.diagonalize( mf1RDM )

    #PING
    ##diagonalize the core-core 1e- hamiltonian, which needs to be non-degenerate, to define a new set of core states which are consistent at each point in time for RT-DMET
    #Ncore                     = Nelec/2 - Nimp
    #core_vecs                 = evecs[ :, -Ncore: ]
    #h_core                    = utils.rot1el( h_site[ Nimp:, Nimp: ], core_vecs )
    #core_evals, new_core_vecs = utils.diagonalize( h_core )
    #new_core_vecs             = np.dot( core_vecs, new_core_vecs )
    #if( core_evals.shape[0] != np.unique( core_evals ).shape[0] ):
    #    print "ERROR: All eigenvalues of core-core 1e- hamiltonian are NOT unique"
    #    print
    #    exit()


    ##diagonalize the virtual-virtual 1e- hamiltonian to define a new set of virtual states which are consistent at each point in time for RT-DMET
    #Nvirt                     = Nbasis - 2*Nimp - Ncore
    #virt_vecs                 = evecs[ :, :Nvirt ]
    #h_virt                    = utils.rot1el( h_site[ Nimp:, Nimp: ], virt_vecs )
    #virt_evals, new_virt_vecs = utils.diagonalize( h_virt )
    #new_virt_vecs             = np.dot( virt_vecs, new_virt_vecs )
    #if( virt_evals.shape[0] != np.unique( virt_evals ).shape[0] ):
    #    print "ERROR: All eigenvalues of virtual-virtual 1e- hamiltonian are NOT unique"
    #    print
    #    exit()

    ##replace original eigenvectors of environment 1RDM with new core and virtual orbitals
    #evecs[ :, :Nvirt ] = np.copy( new_virt_vecs )
    #evecs[ :, -Ncore: ] = np.copy( new_core_vecs )


    #form rotation matrix consisting of unit vectors for impurity and eigenvectors for virtual, bath and core
    rotmat = np.zeros( [ Nbasis, Nimp ] )
    for imp in range(Nimp):

        rotmat[ imp, imp ] = 1.0

        evecs = np.insert( evecs, imp, 0.0, axis=0 )

    rotmat = np.concatenate( (rotmat,evecs), axis=1 )

    return rotmat, evals

#####################################################################

def get_emb_ham( Nimp, Nelec, rotmat, h_site, hamtype, V_site=None ):

    #subroutine to calculate the 1- and 2-electron terms of the hamiltonian in the embedding basis

    Nemb  = h_site.shape[0] #number of embedding states, which is the same as the number of sites
    Ncore = Nelec/2 - Nimp
    Nvirt = Nemb - 2*Nimp - Ncore

    #remove the virtual states from the rotation matrix
    #the rotation matrix is of form ( site basis fcns ) x ( impurities, virtual, bath, core )
    rotmat_small = np.delete( rotmat, np.s_[Nimp:Nimp+Nvirt], 1 )

    #rotate the 1 e- terms, h_emb currently ( impurities, bath, core ) x ( impurities, bath, core )
    h_emb = utils.rot1el( h_site, rotmat_small )

    #rotate the 2 e- terms
    if( hamtype == 0 ):
        #General hamiltonian, V_emb currently ( impurities, bath, core ) ^ 4
        V_emb = utils.rot2el_chem( V_site, rotmat_small )
    elif( hamtype == 1 ):
        #Single impurity anderson model, V_emb ( impurityies, bath ) ^ 4
        size = 2*Nimp
        V_emb = np.zeros( [ size, size, size, size ], dtype=h_emb.dtype )
        V_emb[ 0, 0, 0, 0 ] = V_site

    #augment the impurity/bath 1e- terms from contribution of coulomb and exchange terms btwn impurity/bath and core
    if( hamtype == 0 ):
        #General hamiltonian
        for core in range( 2*Nimp, 2*Nimp+Ncore ):
            h_emb[ :2*Nimp, :2*Nimp ] = h_emb[ :2*Nimp, :2*Nimp ] + 2*V_emb[ :2*Nimp, :2*Nimp, core, core ] - V_emb[ :2*Nimp, core, core, :2*Nimp ]

    #calculate the energy associated with core-core interactions, setting it numerically to a real number since it always will be
    Ecore = 0
    for core1 in range( 2*Nimp, 2*Nimp+Ncore ):

        Ecore += 2*h_emb[ core1, core1 ]

        if( hamtype == 0 ):
            #General hamiltonian
            for core2 in range( 2*Nimp, 2*Nimp+Ncore ):
                Ecore += 2*V_emb[ core1, core1, core2, core2 ] - V_emb[ core1, core2, core2, core1 ]

    Ecore = Ecore.real            


    #shrink h_emb and V_emb arrays to only include the impurity and bath
    h_emb = h_emb[ :2*Nimp, :2*Nimp ]
    if( hamtype == 0 ):
        #General hamiltonian
        V_emb = V_emb[ :2*Nimp, :2*Nimp, :2*Nimp, :2*Nimp ]

    return h_emb, V_emb, Ecore

#####################################################################

def get_new_umat( umat, Nelec, Nbasis, Nimp, h_site, corr1RDM, rotmat, diag_umat, fitbath ):

    #Subroutine to obtain new correlation potential by minimizing difference between correlated and mean-field 1RDM

    #remove the virtual and core states from the rotation matrix
    Nemb  = Nbasis #number of embedding states, which is the same as the number of sites
    Ncore = Nelec/2 - Nimp
    Nvirt = Nemb - 2*Nimp - Ncore
    rotmat_small = np.delete( rotmat, np.s_[Nimp:Nimp+Nvirt], 1 )
    rotmat_small = rotmat_small[ :, :2*Nimp ]

    params = utils.matrix2array( umat, diag_umat )

    gtol = 1e-8
    min_result = minimize( cost_function, params, args=( Nelec, Nimp, h_site, corr1RDM, diag_umat, rotmat_small, fitbath ), method='BFGS', options={'gtol': gtol} )

    while( not min_result.success and gtol < 1e-2 ):
        gtol = gtol*10.0
        min_result = minimize( cost_function, params, args=( Nelec, Nimp, h_site, corr1RDM, diag_umat, rotmat_small, fitbath ), method='BFGS', options={'gtol': gtol} )

    if( not min_result.success ):
        print "ERROR: Minimizing the difference between correlated and mean-field 1RDM failed"
        print
        exit()
    else:
        print "    The difference between correlated and mean-field 1RDM converged to a tolerance of ", gtol


    umat = utils.array2matrix( min_result.x, diag_umat )

    return umat

#####################################################################

def cost_function( params, Nelec, Nimp, h_site, corr1RDM, diag_umat, rotmat, fitbath ):

    #Calculate difference between correlated and mean-field 1RDM in the embedding basis for given correlation potential

    #define current estimate of correlation potential
    umat = utils.array2matrix( params, diag_umat )

    #Calculate modified 1e- terms in site basis
    h_site_mod = np.copy( h_site )
    h_site_mod[ :Nimp, :Nimp ] += umat

    #Run mean-field calculation using modified hamiltonian
    mf1RDM_site, homo_lumo_gap = hf.rhf_calc_hubbard( Nelec, h_site_mod )

    mf1RDM_emb = utils.rot1el( mf1RDM_site, rotmat )

    if( fitbath ):
        return np.linalg.norm( corr1RDM - mf1RDM_emb )**2
    else:
        return np.linalg.norm( corr1RDM[:Nimp,:Nimp] - mf1RDM_emb[:Nimp,:Nimp] )**2

#####################################################################
